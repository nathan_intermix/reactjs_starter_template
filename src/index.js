// inside src/index.js
import React, { Component } from 'react'
import { render } from 'react-dom'

const App = () => {
    return (
        <div>
            <HelloWorld />
            <h1>React App is Running!</h1>
        </div>
    )
}

class HelloWorld extends Component {
    componentDidMount() {
        document.title = "Hello world!"
    }

    render() {
        return (
            <h2>HelloWorld</h2>
        )
    }
}

render(
    <App/>, document.getElementById('root')
)